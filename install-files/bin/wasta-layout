#!/bin/bash

# ==============================================================================
# wasta-layout
#
# This script can be run at any time to "toggle" cinnamon default settings
#   to provide different layout defaults for the system
#
# enabled-applets wasta numbers
#   (will show as #.json in user's ~/.cinnamon/configs/applet-name/ folder):
#
#   menu@cinnamon.org:101
#   show-desktop@cinnamon.org:102
#   panel-launchers@cinnamon.org:103
#   scale@cinnamon.org:104
#   window-list@cinnamon.org:105
#   removable-drives@cinnamon.org:106
#   inhibit@cinnamon.org:107
#   systray@cinnamon.org:108
#   bluetooth@cinnamon.org:109
#   network@cinnamon.org:110
#   sound@cinnamon.org:111
#   power@cinnamon.org:112
#   calendar@simonwiles.net:113
#   windows-quick-list@cinnamon.org:114
#   IcingTaskManager@json:115
#   user@cinnamon.org:116
#   trash@cinnamon.org:117
#   calendar@cinnamon.org:118
#   notifications@cinnamon.org:119
#   grouped-window-list@cinnamon.org:120
#
#   2017-12-24 rik: initial script
#   2018-01-18 rik: default: remove calendar@cinnamon.org so will be re-created
#       with defaults
#       - save and restore worldclocks from calendar@simonwiles.net
#   2018-01-20 rik: resetting all "panels" items that could have multiple
#       panels.  Without this, "cinnamon-settings panel" was crashing.
#   2018-01-20 rik: change cupertino to have shutdown menu on right side
#   2018-05-20 rik: UpdateITMPinned adjusted for Cinnamon 3.8 schema location
#   2018-05-22 rik: resetting button-layout for window controls
#   2019-02-23 rik: updating for cinnamon 4.0: migrate ITM to GWL
#   2019-03-12 rik: correcting prompt for wasta-layout-system
#
# ==============================================================================

# ****TODO: need to reset configs (menu icon, label, etc.) depending on layout, since no longer relying on wasta-layout-system to set the default for the current user.

# ------------------------------------------------------------------------------
# Ensure script running as regular user
# ------------------------------------------------------------------------------
if [ $(id -u) -eq 0 ];
then
    echo "This script should NOT be run with SUDO!  Must be regular user!!"
    echo "Exiting...."
    sleep 5s
    exit 1
fi

# ------------------------------------------------------------------------------
# Setup
# ------------------------------------------------------------------------------
echo
echo " *** Script Entry: wasta-layout for user: $USER"
echo
# Check for input parameter
CHOICE=$1
AUTO=""

if [ "$CHOICE" ];
then
    case "$CHOICE" in
    default|redmond7|cupertino|unity)
        echo
        echo "*** wasta-layout called with valid layout: $CHOICE"
        echo
        AUTO="auto"
    ;;

    *)
        echo
        echo "*** wasta-layout called with invalid layout $CHOICE"
        echo
        CHOICE=""
    ;;
    esac
fi

DIR=/usr/share/wasta-cinnamon-layout
CONFIG_DIR=/home/$USER/.config/wasta-layout
mkdir -p $CONFIG_DIR

PREV_LAUNCHERS=""
PREV_LAUNCHERS_FILE=$CONFIG_DIR/wasta-layout-launchers.txt
if [ -e $PREV_LAUNCHERS_FILE ];
then
    PREV_LAUNCHERS=$(cat $PREV_LAUNCHERS_FILE)
    echo
    echo "*** PREV_LAUNCHERS: $PREV_LAUNCHERS"
    echo
fi

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

#updateITMPinned()
#{
#    #if SAVED_LAUNCHERS set pinned-apps
#    if [ "$SAVED_LAUNCHERS" ];
#    then
#        # ITM has different configs for different cinnamon versions
#        # 2018-04: Identical for 3.4 and 3.8
#        DEFAULT_CONFIG=/usr/share/cinnamon/applets/IcingTaskManager@json/3.8/settings-schema.json
#        JSON_DIR=/home/$USER/.cinnamon/configs/IcingTaskManager@json
#        # NOTE: #.json has to match enabled-applets number!!
#        JSON_FILE=$JSON_DIR/115.json
#
#        if ! [ -e "$JSON_FILE" ];
#        then
#            echo "copying in default json config: $DEFAULT_CONFIG"
#            mkdir -p $JSON_DIR 
#            cp $DEFAULT_CONFIG $JSON_FILE
#        fi
#
#        #now update saved launchers for ALL instances (hopefully not going to make anyone angry...)
#        echo "updating config for JSON_FILE: $JSON_FILE"
#        #jq can't do "sed -i" inplace update, so need to re-create file, then
#        # update ownership (in case run as root)
#        NEW_FILE=$(jq '.["pinned-apps"].value='"$SAVED_LAUNCHERS" < $JSON_FILE)
#        echo "$NEW_FILE" > $JSON_FILE
#    fi
#}

updateGWLPinned()
{
    #if SAVED_LAUNCHERS set pinned-apps
    if [ "$SAVED_LAUNCHERS" ];
    then
        DEFAULT_CONFIG=/usr/share/cinnamon/applets/grouped-window-list@cinnamon.org/settings-schema.json
        JSON_DIR=/home/$USER/.cinnamon/configs/grouped-window-list@cinnamon.org
        # NOTE: #.json has to match enabled-applets number!!
        JSON_FILE=$JSON_DIR/120.json

        if ! [ -e "$JSON_FILE" ];
        then
            echo
            echo "*** copying in default json config: $DEFAULT_CONFIG"
            echo
            mkdir -p $JSON_DIR
            cp $DEFAULT_CONFIG $JSON_FILE
        fi

        #update saved launchers
        echo
        echo "*** updating JSON_FILE: $JSON_FILE"
        echo
        #jq can't do "sed -i" inplace update, so need to re-create file
        NEW_FILE=$(jq '.["pinned-apps"].value='"$SAVED_LAUNCHERS" < $JSON_FILE)
        echo "$NEW_FILE" > $JSON_FILE
    fi
}

updateWorldClocks()
{
    if [ "$SAVED_WC" ];
    then
        DEFAULT_CONFIG=/usr/share/cinnamon/applets/calendar@simonwiles.net/settings-schema.json
        JSON_DIR=/home/$USER/.cinnamon/configs/calendar@simonwiles.net
        # NOTE: #.json has to match enabled-applets number!!
        JSON_FILE=$JSON_DIR/113.json

        if ! [ -e "$JSON_FILE" ];
        then
            echo
            echo "*** copying in default json config: $DEFAULT_CONFIG"
            echo
            mkdir -p $JSON_DIR
            cp $DEFAULT_CONFIG $JSON_FILE
        fi

        #update saved launchers
        echo
        echo "*** updating JSON_FILE: $JSON_FILE"
        echo
        echo "updating config for JSON_FILE: $JSON_FILE"
        #jq can't do "sed -i" inplace update, so need to re-create file
        NEW_FILE=$(jq '.["worldclocks"].value='"$SAVED_WC" < $JSON_FILE)
        # read -p "$NEW_FILE"
        echo "$NEW_FILE" > $JSON_FILE
    fi
}

resetMenu()
{
    # menu applet reset
    DEFAULT_CONFIG=/usr/share/cinnamon/applets/menu@cinnamon.org/settings-schema.json
    JSON_DIR=/home/$USER/.cinnamon/configs/menu@cinnamon.org
    # NOTE: #.json has to match enabled-applets number!!
    JSON_FILE=$JSON_DIR/101.json

    mkdir -p $JSON_DIR
    cp $DEFAULT_CONFIG $JSON_FILE

    # reset to show favorite / shutdown options
    # reset to not use custom menu icon
    # reset menu icon value
    # reset label to "Menu"
    echo
    echo "*** Resetting JSON_FILE:"
    echo "      $JSON_FILE"
    echo
    #jq can't do "sed -i" inplace update, so need to re-create file, then
    # update ownership (in case run as root)
    NEW_FILE=$(jq '.["favbox-show"].value=true | .["menu-custom"].value=true | .["menu-icon"].value="'/usr/share/cinnamon/theme/menu-symbolic.svg'" | .["menu-label"].value="Menu"' \
        < $JSON_FILE)
    echo "$NEW_FILE" > $JSON_FILE
}

removeMenuFavs()
{
    JSON_DIR=/home/$USER/.cinnamon/configs/menu@cinnamon.org
    # NOTE: #.json has to match enabled-applets number!!
    JSON_FILE=$JSON_DIR/101.json
    # hide favorite / shutdown options
    # use custom menu icon
    # remove label
    echo
    echo "*** Removing Favorites and Session options from JSON_FILE:"
    echo "      $JSON_FILE"
    echo
    #jq can't do "sed -i" inplace update, so need to re-create file, then
    # update ownership (in case run as root)
    NEW_FILE=$(jq '.["favbox-show"].value=false' \
        < $JSON_FILE)
    echo "$NEW_FILE" > $JSON_FILE
}

changeMenuIcon()
{
    JSON_DIR=/home/$USER/.cinnamon/configs/menu@cinnamon.org
    # NOTE: #.json has to match enabled-applets number!!
    JSON_FILE=$JSON_DIR/101.json
    # use custom menu icon
    # remove label
    echo
    echo "*** Changing Menu Icon for JSON_FILE:"
    echo "      $JSON_FILE"
    echo
    #jq can't do "sed -i" inplace update, so need to re-create file, then
    # update ownership (in case run as root)
    NEW_FILE=$(jq '.["menu-custom"].value=true | .["menu-icon"].value="'$DIR/resources/launcher_bfb.png'" | .["menu-label"].value=""' \
        < $JSON_FILE)
    echo "$NEW_FILE" > $JSON_FILE
}

# ------------------------------------------------------------------------------
# Main Processing
# ------------------------------------------------------------------------------

# SAVE current GWL OR panel-launcher apps to restore later....
SAVED_LAUNCHERS=""

PL_FILE=/home/$USER/.cinnamon/configs/panel-launchers@cinnamon.org/103.json
if [ -e "$PL_FILE" ];
then
    PL_LAUNCHERS=$(jq -c '.["launcherList"].value' < $PL_FILE)
fi

# legacy: remove for 20.04
ITM_FILE=/home/$USER/.cinnamon/configs/IcingTaskManager@json/115.json
if [ -e "$ITM_FILE" ];
then
    ITM_PINNED_APPS=$(jq -c '.["pinned-apps"].value' < $ITM_FILE)
fi


GWL_FILE=/home/$USER/.cinnamon/configs/grouped-window-list@cinnamon.org/120.json
if [ -e "$GWL_FILE" ];
then
    GWL_PINNED_APPS=$(jq -c '.["pinned-apps"].value' < $GWL_FILE)
fi

# legacy: remove for 20.04
# migration from ITM to GWL
if [ "$ITM_FILE" -nt "$GWL_FILE" ];
then
    echo
    echo "*** Migrating ITM Pinned Apps to GWL"
    echo
    mkdir -p "/home/$USER/.cinnamon/configs/grouped-window-list@cinnamon.org"
    # copy ITM to GWL, preserving date/time
    cp -p $ITM_FILE $GWL_FILE
    GWL_PINNED_APPS=$ITM_PINNED_APPS
fi

if [ "$PL_FILE" -nt "$GWL_FILE" ];
then
    echo
    echo "*** PL newest"
    SAVED_LAUNCHERS=$PL_LAUNCHERS
    echo "*** SAVED_LAUNCHERS set to: $PL_LAUNCHERS"
    echo
elif [ "$GWL_FILE" -nt "$PL_FILE" ];
then
    echo
    echo "*** GWL newest"
    SAVED_LAUNCHERS=$GWL_PINNED_APPS
    echo "*** SAVED_LAUNCHERS set to: $GWL_PINNED_APPS"
    echo
else
    echo
    echo "*** PL and GWL don't exist..... both equal implying only at default"
    # Attempt to set to saved file $PREV_LAUNCHERS
    if [ "$PREV_LAUNCHERS" ];
    then
        SAVED_LAUNCHERS=$PREV_LAUNCHERS
        echo "*** SAVED_LAUNCHERS set from PREV_LAUNCHERS_FILE: $PREV_LAUNCHERS"
    fi
    echo
fi

#if [ "$PL_FILE" -nt "$ITM_FILE" ];
#then
#    echo "*** PL newest"
#    SAVED_LAUNCHERS=$PL_LAUNCHERS
#    echo "*** SAVED_LAUNCHERS set to: $PL_LAUNCHERS"
#elif [ "$ITM_FILE" -nt "$PL_FILE" ];
#then
#    echo "*** ITM newest"
#    SAVED_LAUNCHERS=$ITM_PINNED_APPS
#    echo "*** SAVED_LAUNCHERS set to: $ITM_PINNED_APPS"
#else
#    echo "*** PL and ITM don't exist..... both equal implying only at default"
#fi

echo
echo "*** SAVED_LAUNCHERS final value: $SAVED_LAUNCHERS"
echo

# Write PREV_LAUNCHERS_FILE (if cupertino used then PL and GWL will be deleted)
if [ "$SAVED_LAUNCHERS" ];
then
    echo "$SAVED_LAUNCHERS" > $PREV_LAUNCHERS_FILE
fi

# SAVE current world clocks
SAVED_WC=""

WC_FILE=/home/$USER/.cinnamon/configs/calendar@simonwiles.net/113.json
if [ -e "$WC_FILE" ];
then
    SAVED_WC=$(jq -c '.["worldclocks"].value' < $WC_FILE)
    echo
    echo "*** SAVED_WC set to: $SAVED_WC"
    echo
fi

# prompt user if $CHOICE not yet set
if ! [ "$AUTO" ];
then
    CHOICE=$(zenity --list --title="wasta [Layout]" \
        --window-icon=/usr/share/icons/hicolor/64x64/apps/wasta-linux.png \
        --height=300 --text \
"This utility <i><b>changes</b></i> the layout of the <i>panel</i> (taskbar) and other
items of the Cinnamon Desktop Interface.

<b><i>Choose your preferred layout:</i></b>" --radiolist \
        --column "" --column "Layout" --column "Description" \
        TRUE        default           "Default Cinnamon layout" \
        FALSE       redmond7          "Windows 7 inspired layout" \
        FALSE       cupertino         "MacOS/OSX insipired layout" \
        FALSE       unity             "Ubuntu Unity inspired layout")
fi

if [ "$CHOICE" == "" ];
then
    # EXIT: no layout selected
    echo
    echo "*** no layout selected: exiting..."
    echo
    exit 0
fi

# all layouts: stop plank and disable any user-level plank
# autostart (cupertino will later re-enable)
killall plank > /dev/null 2>&1 || true;

PLANK_FILE=/home/$USER/.config/autostart/plank-wasta-layout.desktop
if [ -e $PLANK_FILE ];
then
    desktop-file-edit --set-key="X-GNOME-Autostart-enabled" --set-value=false \
        $PLANK_FILE
fi

case "$CHOICE" in

"default")
    # menu@cinnamon.org:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/menu@cinnamon.org
    rm -rf /home/$USER/.cinnamon/configs/menu@cinnamon.org

    # reset menu applet to default:
    resetMenu

    # calendar@cinnamon.org:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/calendar@cinnamon.org
    rm -rf /home/$USER/.cinnamon/configs/calendar@cinnamon.org

    # if SAVED_LAUNCHERS set panel-launchers
    if [ "$SAVED_LAUNCHERS" ];
    then
        DEFAULT_CONFIG=/usr/share/cinnamon/applets/panel-launchers@cinnamon.org/settings-schema.json
        JSON_DIR=/home/$USER/.cinnamon/configs/panel-launchers@cinnamon.org
        # NOTE: #.json has to match enabled-applets number!!
        JSON_FILE=$JSON_DIR/103.json

        if ! [ -e "$JSON_FILE" ];
        then
            echo
            echo "*** copying in default json config: $DEFAULT_CONFIG"
            echo
            mkdir -p $JSON_DIR
            cp $DEFAULT_CONFIG $JSON_FILE
        fi

        #update saved launchers
        echo
        echo "*** updating JSON_FILE: $JSON_FILE"
        echo
        #jq can't do "sed -i" inplace update, so need to re-create file, then
        # update ownership (in case run as root)
        NEW_FILE=$(jq '.["launcherList"].value='"$SAVED_LAUNCHERS" < $JSON_FILE)
        echo "$NEW_FILE" > $JSON_FILE
    fi

    # need to set keys associated with panel layouts
    gsettings set org.cinnamon enabled-applets "['panel1:left:0:menu@cinnamon.org:101', 'panel1:left:1:show-desktop@cinnamon.org:102', 'panel1:left:2:panel-launchers@cinnamon.org:103', 'panel1:left:3:window-list@cinnamon.org:105', 'panel1:right:1:removable-drives@cinnamon.org:106', 'panel1:right:2:inhibit@cinnamon.org:107', 'panel1:right:3:systray@cinnamon.org:108', 'panel1:right:4:bluetooth@cinnamon.org:109', 'panel1:right:5:network@cinnamon.org:110', 'panel1:right:6:sound@cinnamon.org:111', 'panel1:right:7:power@cinnamon.org:112', 'panel1:right:8:calendar@cinnamon.org:118', 'panel1:right:9:windows-quick-list@cinnamon.org:114']"
    gsettings set org.cinnamon enabled-extensions "[]"
    gsettings set org.cinnamon.desktop.wm.preferences button-layout ':minimize,maximize,close'
    # all "panels" items need reset since number of panels could have changed
    gsettings set org.cinnamon panel-zone-icon-sizes '[{"panelId":1,"left":0,"center":0,"right":24}]'
    gsettings set org.cinnamon panels-autohide "['1:false']"
    gsettings set org.cinnamon panels-enabled "['1:0:bottom']"
    gsettings set org.cinnamon panels-height "['1:28']"
    gsettings set org.cinnamon panels-hide-delay "['1:0']"
    gsettings set org.cinnamon panels-show-delay "['1:0']"
;;

"redmond7")
    # menu@cinnamon.org:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/menu@cinnamon.org
    rm -rf /home/$USER/.cinnamon/configs/menu@cinnamon.org

    # reset Main Menu
    resetMenu

    # legacy: remove for 20.04
    # IcingTaskManager@json:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/IcingTaskManager@json
    rm -rf /home/$USER/.cinnamon/configs/IcingTaskManager@json

    # grouped-window-list@cinnamon.org:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/grouped-window-list@cinnamon.org
    rm -rf /home/$USER/.cinnamon/configs/grouped-window-list@cinnamon.org

    # calendar@simonwiles.net:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/calendar@simonwiles.net
    rm -rf /home/$USER/.cinnamon/configs/calendar@simonwiles.net

    # if SAVED_LAUNCHERS set pinned-apps
    updateGWLPinned

    # if SAVED_WC set worldclocks
    updateWorldClocks

    # need to set keys associated with panel layouts
    gsettings set org.cinnamon enabled-applets "['panel1:left:0:menu@cinnamon.org:101', 'panel1:left:1:show-desktop@cinnamon.org:102', 'panel1:left:3:grouped-window-list@cinnamon.org:120', 'panel1:right:1:removable-drives@cinnamon.org:106', 'panel1:right:2:inhibit@cinnamon.org:107', 'panel1:right:3:systray@cinnamon.org:108', 'panel1:right:4:bluetooth@cinnamon.org:109', 'panel1:right:5:network@cinnamon.org:110', 'panel1:right:6:sound@cinnamon.org:111', 'panel1:right:7:power@cinnamon.org:112', 'panel1:right:8:calendar@simonwiles.net:113', 'panel1:right:9:windows-quick-list@cinnamon.org:114']"
    gsettings set org.cinnamon enabled-extensions "[]"
    gsettings set org.cinnamon.desktop.wm.preferences button-layout ':minimize,maximize,close'
    # all "panels" items need reset since number of panels could have changed
    gsettings set org.cinnamon panel-zone-icon-sizes '[{"panelId":1,"left":0,"center":0,"right":24}]'
    gsettings set org.cinnamon panels-autohide "['1:false']"
    gsettings set org.cinnamon panels-enabled "['1:0:bottom']"
    gsettings set org.cinnamon panels-height "['1:35']"
    gsettings set org.cinnamon panels-hide-delay "['1:0']"
    gsettings set org.cinnamon panels-show-delay "['1:0']"
;;

"unity")
    # menu@cinnamon.org:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/menu@cinnamon.org
    rm -rf /home/$USER/.cinnamon/configs/menu@cinnamon.org

    # reset Main Menu
    resetMenu
    # remove Favorites and Session options from Main Menu
    removeMenuFavs
    # set bfb icon and no label for Main Menu
    changeMenuIcon

    # legacy: remove for 20.04
    # IcingTaskManager@json:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/IcingTaskManager@json
    rm -rf /home/$USER/.cinnamon/configs/IcingTaskManager@json

    # grouped-window-list@cinnamon.org:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/grouped-window-list@cinnamon.org
    rm -rf /home/$USER/.cinnamon/configs/grouped-window-list@cinnamon.org

    # calendar@simonwiles.net:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/calendar@simonwiles.net
    rm -rf /home/$USER/.cinnamon/configs/calendar@simonwiles.net

    # transparent-panels@germanfr
    #   - remove any local extension and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/extensions/transparent-panels@germanfr
    rm -rf /home/$USER/.cinnamon/configs/transparent-panels@germanfr

    # if SAVED_LAUNCHERS set pinned-apps
    updateGWLPinned

    # if SAVED_WC set worldclocks
    updateWorldClocks

    # need to set keys associated with panel layouts
    gsettings set org.cinnamon enabled-applets "['panel1:right:1:removable-drives@cinnamon.org:106', 'panel1:right:2:inhibit@cinnamon.org:107', 'panel1:right:3:systray@cinnamon.org:108', 'panel1:right:4:bluetooth@cinnamon.org:109', 'panel1:right:5:network@cinnamon.org:110', 'panel1:right:6:sound@cinnamon.org:111', 'panel1:right:7:power@cinnamon.org:112', 'panel1:right:8:calendar@simonwiles.net:113', 'panel1:right:9:user@cinnamon.org:116', 'panel2:left:0:menu@cinnamon.org:101', 'panel2:left:1:show-desktop@cinnamon.org:102', 'panel2:left:2:grouped-window-list@cinnamon.org:120', 'panel2:right:1:trash@cinnamon.org:117']"
    gsettings set org.cinnamon enabled-extensions "['transparent-panels@germanfr']"
    gsettings set org.cinnamon.desktop.wm.preferences button-layout ':minimize,maximize,close'
    # all "panels" items need reset since number of panels could have changed
    gsettings set org.cinnamon panel-zone-icon-sizes '[{"panelId":1,"left":0,"center":0,"right":24},{"panelId":2,"left":0,"center":0,"right":0}]'
    gsettings set org.cinnamon panels-autohide "['1:false', '2:false']"
    gsettings set org.cinnamon panels-enabled "['1:0:top', '2:0:left']"
    gsettings set org.cinnamon panels-height "['1:28', '2:40']"
    gsettings set org.cinnamon panels-hide-delay "['1:0', '2:0']"
    gsettings set org.cinnamon panels-show-delay "['1:0', '2:0']"
;;

"cupertino")
    # menu@cinnamon.org:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/menu@cinnamon.org
    rm -rf /home/$USER/.cinnamon/configs/menu@cinnamon.org

    # reset Main Menu
    resetMenu
    # remove Favorites and Session options from Main Menu
    # rik: not enabling for now so as to not confuse users... but it is
    # "cleaner looking" with favorties removed
    # removeMenuFavs

    # calendar@simonwiles.net:
    #   - remove any local applet and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/applets/calendar@simonwiles.net
    rm -rf /home/$USER/.cinnamon/configs/calendar@simonwiles.net

    # transparent-panels@germanfr
    #   - remove any local extension and configs (will be re-created from defaults)
    rm -rf /home/$USER/.local/share/cinnamon/extensions/transparent-panels@germanfr
    rm -rf /home/$USER/.cinnamon/configs/transparent-panels@germanfr

    PLANK_FOLDER=/home/$USER/.config/plank/dock1/launchers
    if ! [ -e "$PLANK_FOLDER" ];
    then
        echo
        echo "*** Creating default plank launcher folder"
        echo
        mkdir -p $PLANK_FOLDER
        rsync -av $DIR/resources/launchers/ $PLANK_FOLDER/
        sleep 1
    fi

    # reset keys associated with plank
    gsettings reset net.launchpad.plank.dock.settings:/ hide-mode
    gsettings reset net.launchpad.plank.dock.settings:/ show-dock-item
    gsettings reset net.launchpad.plank.dock.settings:/ theme
    gsettings reset net.launchpad.plank.dock.settings:/ zoom-enabled
    gsettings reset net.launchpad.plank.dock.settings:/ zoom-percent
        
    # start Plank
    nohup plank &> /dev/null &

    # set Plank to autostart
    if ! [ -e $PLANK_FILE ];
    then
        echo
        echo "*** Creating plank autostart file"
        echo
        cp /usr/share/applications/plank.desktop $PLANK_FILE
        desktop-file-edit --add-only-show-in="X-Cinnamon" $PLANK_FILE
        desktop-file-edit --set-name="Plank (Wasta-Layout)" $PLANK_FILE
    fi
    # ensure plank will autostart for user
    desktop-file-edit --set-key="X-GNOME-Autostart-enabled" --set-value=true \
        $PLANK_FILE

    # if SAVED_WC set worldclocks
    updateWorldClocks

    # need to set keys associated with panel layouts
    gsettings set org.cinnamon enabled-applets "['panel1:left:0:menu@cinnamon.org:101', 'panel1:right:1:removable-drives@cinnamon.org:106', 'panel1:right:2:inhibit@cinnamon.org:107', 'panel1:right:3:systray@cinnamon.org:108', 'panel1:right:4:bluetooth@cinnamon.org:109', 'panel1:right:5:network@cinnamon.org:110', 'panel1:right:6:sound@cinnamon.org:111', 'panel1:right:7:power@cinnamon.org:112', 'panel1:right:8:calendar@simonwiles.net:113', 'panel1:right:9:user@cinnamon.org:116']"
    gsettings set org.cinnamon enabled-extensions "['transparent-panels@germanfr']"
    gsettings set org.cinnamon.desktop.wm.preferences button-layout 'close,minimize,maximize:'
    # all "panels" items need reset since number of panels could have changed
    gsettings set org.cinnamon panel-zone-icon-sizes '[{"panelId":1,"left":0,"center":0,"right":24}]'
    gsettings set org.cinnamon panels-autohide "['1:false']"
    gsettings set org.cinnamon panels-enabled "['1:0:top']"
    gsettings set org.cinnamon panels-height "['1:28']"
    gsettings set org.cinnamon panels-hide-delay "['1:0']"
    gsettings set org.cinnamon panels-show-delay "['1:0']"
;;

*)
    #do nothing
;;

esac

RETURN=$?
if [ $RETURN == 0 ];
then
    MSG+="Wasta-Layout user interface updated to: $CHOICE\n\n"
    echo "$CHOICE" > $CONFIG_DIR/current-wasta-layout.txt
else
    MSG+="Error: Wasta-Layout user interface not updated\n\n"
fi

sleep 1s

if [ "$XDG_SESSION_DESKTOP" == "cinnamon" ];
then
    echo
    echo "*** Restarting Cinnamon ***"
    echo
    MSG+="Restarting Cinnamon to Apply Changes\n\n"

    # restart cinnamon
    nohup cinnamon --replace &> /dev/null &

    # give a few seconds for Cinnamon to finish restarting
    sleep 3s

    echo
    echo "*** Cinnamon restarted ***"
    echo
    MSG+="Cinnamon Restarted Successfully\n\n"
else
    # cinnamon NOT active, don't attempt restart
    MSG+="Cinnamon not active so no need to restart\n\n"
fi

# ------------------------------------------------------------------------------
# Optionally run wasta-system-layout
# ------------------------------------------------------------------------------

# prompt for running wasta-layout-system
if ! [ "$AUTO" ];
then
    zenity --question --title "wasta [Layout]: Update System Default?" \
        --no-wrap --height=150 --width=450 \
        --window-icon=/usr/share/icons/hicolor/64x64/apps/wasta-linux.png \
        --text="<b>Update Wasta-Layout system default to $CHOICE?</b>

<i>NOTE: you will need to provide an administrative password</i>"
    SET_DEFAULT=$?

    if [ "$SET_DEFAULT" == 0 ];
    then
        pkexec /usr/bin/wasta-layout-system $CHOICE
        RETURN=$?
        if [ "$RETURN" -ne 0 ];
        then
            # didn't return clean from wasta-layout-system: get outta here!
            exit 1
        fi
        MSG+="Wasta-Layout system default updated to: $CHOICE\n\n"
    else
        MSG+="Wasta-Layout system default not updated\n\n"
    fi
fi

# ------------------------------------------------------------------------------
# Finished
# ------------------------------------------------------------------------------

MSG+="<b>Please reboot your computer if you have any trouble</b>\n\n"

echo
echo "*** output messages:"
echo "$MSG"
echo "*** wasta-layout finished"
echo

# prompt user
if ! [ "$AUTO" ];
then
    zenity --info --title "wasta [Layout]: Finished" --no-wrap --height=150 --width=450 \
        --window-icon=/usr/share/icons/hicolor/64x64/apps/wasta-linux.png \
        --text="$MSG"
fi

exit 0
